const fs = require('fs');

class JsonStorage {

    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        return this.jsonArray.nextId;
    }

    incrementNextId() {
        this.jsonArray.nextId++;
        this.writeItems(this.jsonArray.items);
        return this.jsonArray.nextId;
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        this.jsonArray = JSON.parse(jsonText.toString());
        return this.jsonArray;
    }

    writeItems(items) {
        this.jsonArray.items = items;
        const jsonText = JSON.stringify(this.jsonArray, null, 4);
        fs.writeFileSync(this.filePath, jsonText);
    }
};

module.exports = JsonStorage;