const providerRouter = require('express').Router();
const providerController = require('../controllers/providerController');

providerRouter
    /**
     * Get all providers 
     * @route GET /api/providers
     * @group Providers - provider operations
     * @param {integer} page.query - serial number of the subscription
     * @param {integer} per_page.query - number of records per page
     * @returns {Array<Provider>} 200 - Providers array
     * @returns {Error} 404 - Bad request
     */
    .get("/", providerController.getProviders)
    /**
     * Get provider by id
     * @route GET /api/providers/{id}
     * @group Providers - provider operations
     * @param {integer} id.path.required - id of the Provider eg:1
     * @returns {Provider.model} 200 - Provider object
     * @returns {Error} 404 - Provider not found
     */
    .get("/:id(\\d+)", providerController.getProviderById)
    /**
     * Add provider
     * @route POST /api/providers
     * @group Providers - provider operations
     * @param {Provider.model} providerModel.body.required - Provider model
     * @returns {Provider.model} 201 - Provider object created
     * @returns {Error} 400 - Bad request
     */
    .post("/", providerController.addProvider)
    /**
     * Delete provider by id
     * @route DELETE /api/providers/{id}
     * @group Providers - provider operations
     * @param {integer} id.path.required - id of the Provider eg:1
     * @returns {Provider.model} 200 - Provider object deleted
     * @returns {Error} 404 - Provider not found (id doesn't exist)
     */
    .delete("/:id(\\d+)", providerController.deleteProvider)
    /**
     * Update provider by id
     * @route PUT /api/providers/{id}
     * @group Providers - provider operations
     * @param {integer} id.path.required - id of the Provider which you want to update
     * @param {Provider.model} providerModel.body.required - Provider model
     * @returns {Provider.model} 200 - Provider object updated
     * @returns {Error} 404 - not Found (id doesn't exist)
     * @returns {Error} 400 - Bad request (without parameters)
     */
    .put("/:id(\\d+)", providerController.updateProvider);

module.exports = providerRouter;