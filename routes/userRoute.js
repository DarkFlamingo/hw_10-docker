const userRouter = require('express').Router();
const userController = require('../controllers/userContoller');

userRouter
    /**
     * Get user by id
     * @route GET /api/users/{id}
     * @group Users - user operations
     * @param {integer} id.path.required - id of the User eg:1
     * @returns {User.model} 200 - User object
     * @returns {Error} 404 - User not found
     */
    .get("/:id(\\d+)", userController.getUserById)
    /**
     * Get all users 
     * @route GET /api/users
     * @group Users - user operations
     * @param {integer} page.query - serial number of the subscription
     * @param {integer} per_page.query - number of records per page
     * @returns {Array<User>} 200 - Users array
     * @returns {Error} 404 - Bad request
     */
    .get("/", userController.getUsers);

module.exports = userRouter;
