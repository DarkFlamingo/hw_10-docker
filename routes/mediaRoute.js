const mediaRouter = require('express').Router();
const mediaController = require('../controllers/mediaController');

mediaRouter
    /**
     * Get media by id
     * @route GET /api/media/{id}
     * @group Media - media operations
     * @param {integer} id.path.required - id of the Media eg:1
     * @returns {string} 200 - File
     * @returns {Error} 404 - Media not found
     */
    .get("/:media_id(\\d+)", mediaController.getMediaById)
    /**
     * Get media by id
     * @route POST /api/media
     * @group Media - media operations
     * @consumes multipart/form-data
     * @param {file} image.formData.required - uploaded image
     * @returns {integer} 200 - File
     * @returns {Error} 400 - Bad request
     * @returns {Error} 500 - Internal Server Error
     */
    .post("/", mediaController.addMedia);

module.exports = mediaRouter;