const apiRouter = require('express').Router();
const userRouter = require('./userRoute');
const providerRouter = require('./providerRoute');
const mediaRouter = require('./mediaRoute');

apiRouter
    .use("/users", userRouter)
    .use("/providers", providerRouter)
    .use("/media", mediaRouter);

module.exports = apiRouter;
