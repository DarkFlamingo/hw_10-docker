const express = require('express');
const app = express();

const apiRouter = require('./routes/apiRoute');

const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);

const options = {
  swaggerDefinition: {
    info: {
      description: 'description',
      title: 'title',
      version: '1.0.0',
    },
    host: 'localhost:3000',
    produces: ['application/json'],
  },
  basedir: __dirname,
  files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(options);

app.use('/', (req, res) => res.send('Welcome to user-provider api'));

app.use('/api', apiRouter);

app.use((req, res) => {
  res.status(400).json({ message: 'Wrong routing in request' });
});

app.listen(80, () => {
  console.log('Server is ready');
});
