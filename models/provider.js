
/**
 * @typedef Provider
 * @property {integer} id - unique field id
 * @property {string} name.required - name
 * @property {string} connectionType.required - connection type
 * @property {integer} price.required - price 
 * @property {integer} rating.required - providers's rating
 * @property {string} date.required - date of creation (ISO8601)
 */

class Provider {

    constructor(id, name, connectionType, price, rating, date) {
        this.id = id;  // number
        this.name = name;  // string 
        this.connectionType = connectionType;  // string
        this.price = price;  // number
        this.rating = rating;  // number
        this.date = date;  // string
    }
};

module.exports = Provider;