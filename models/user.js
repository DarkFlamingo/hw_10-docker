
/**
 * @typedef User
 * @property {integer} id - unique field id
 * @property {string} login.required - login
 * @property {string} fullname.required - user's fullname
 * @property {integer} role.required - role in the system (0 - simple user, 1 - administrator)
 * @property {string} registeredAt.required - registration date (ISO8601)
 * @property {string} avaUrl.required - avatar URL
 * @property {boolean} isEnabled.required - is enabled
 */

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;  // number (0 - simple user, 1 - administrator)
        this.registeredAt = registeredAt;  // string;
        this.avaUrl = avaUrl;  // image URL;
        this.isEnabled = isEnabled;  // bool;
    }
};

module.exports = User;