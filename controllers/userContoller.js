const UserRepository = require('./../repositories/userRepository');
const userRepository = new UserRepository('./data/users.json');

const pageSize = 5;

module.exports = {
    getUsers(req, res) {
        let page;
        let per_page;
        if(req.query.page == undefined) {
            page = 1;
        } else {
            page = Number.parseInt(req.query.page);
        }
        if(req.query.per_page == undefined) {
            per_page = pageSize;
        }
        else if(Number.parseInt(req.query.per_page) > 100) {
            per_page = 100;
        } else {
            per_page = Number.parseInt(req.query.per_page);
        }
        const lost_pages = (page - 1)*per_page;
        const users = userRepository.getUsers();
        res.status(200).json(users.slice(lost_pages, lost_pages + per_page));
    },

    getUserById(req, res) {
        const provider = userRepository.getUserById(parseInt(req.params.id));
        if(provider == null) {
            res.status(404).json({message:"Not Found"});
        } else {
            res.status(200).json(provider);
        }
    },
};
