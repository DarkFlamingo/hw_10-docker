const ProviderRepository = require('./../repositories/providerRepository');
const Provider = require('./../models/provider');
const moment = require('moment');

const providerRepository = new ProviderRepository('./data/providers.json')

const pageSize = 5;

function checkDataForAdd(obj) {
    if(obj.id == undefined) {
        obj.id = 0;
    } else {
        if(typeof(obj.id) != 'number') {
            if (parseInt(obj.id) == NaN) {
                return null;
            } else {
                obj.id = parseInt(obj.id);
            }
        }
    }

    if(obj.name == undefined || obj.connectionType == undefined || typeof(obj.name) != 'string' || typeof(obj.connectionType) != 'string') {
        return null;
    }

    if(obj.price != undefined) {
        if(typeof(obj.price) != 'number') {
            if(parseInt(obj.price) == NaN) {
                return null;
            } else {
                obj.price = parseInt(obj.price);
            }
        }
    } else {
        return null;
    }

    if(obj.rating != undefined) {
        if(typeof(obj.rating) != 'number') {
            if (parseInt(obj.rating) == NaN) {
                return null;
            } else {
                obj.rating = parseInt(obj.rating);
            }
        }
    } else {
        return null;
    }

    if(obj.date == undefined || typeof(obj.date) != 'string' || !(moment(obj.date, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
        return null;
    }

    return new Provider(obj.id, obj.name, obj.connectionType, obj.price, obj.rating, obj.date);
}

function checkDataForUpdate(oldProvider, newProvider) {
    if(newProvider.name == undefined) {
        newProvider.name = oldProvider.name;
    } else {
        if(typeof(newProvider.name) != 'string') {
            return null;
        }
    }

    if(newProvider.connectionType == undefined) {
        newProvider.connectionType = oldProvider.connectionType;
    } else {
        if(typeof(newProvider.connectionType) != 'string') {
            return null;
        }
    }

    if(newProvider.price == undefined) {
        newProvider.price = oldProvider.price;
    } else {
        if(typeof(newProvider.price) != 'number') {
            if(parseInt(newProvider.price) == NaN) {
                return null;
            } else {
                newProvider.price = parseInt(newProvider.price);
            }
        }
    }

    if(newProvider.rating == undefined) {
        newProvider.rating = oldProvider.rating;
    } else {
        if(typeof(newProvider.rating) != 'number') {
            if (parseInt(newProvider.rating) == NaN) {
                return null;
            } else {
                newProvider.rating = parseInt(newProvider.rating);
            }
        }
    }

    if(newProvider.date == undefined) {
        newProvider.date = oldProvider.date;
    } else {
        if(typeof(newProvider.date) != 'string' || !(moment(newProvider.date, "YYYY-MM-DDTHH:mm:ss", true).isValid())) {
            return null;
        }
    }

    return newProvider;
}

module.exports = {
    getProviders(req, res) {
        let page;
        let per_page;
        if(req.query.page == undefined) {
            page = 1;
        } else {
            page = Number.parseInt(req.query.page);
        }
        if(req.query.per_page == undefined) {
            per_page = pageSize;
        }
        else if(Number.parseInt(req.query.per_page) > 100) {
            per_page = 100;
        } else {
            per_page = Number.parseInt(req.query.per_page);
        }
        const lost_pages = (page - 1)*per_page;
        let providers = providerRepository.getProviders();
        res.status(200).json(providers.slice(lost_pages, lost_pages + per_page));
    },

    getProviderById(req, res) {
        let provider = providerRepository.getProviderById(Number.parseInt(req.params.id));
        if(provider == null) {
            res.status(404).json({message:"Not Found"});
        } else {
            res.status(200).json(provider);
        }
    },

    addProvider(req, res) {
        let newProvider = checkDataForAdd(req.body);
        if(newProvider == null) {
            res.status(400).json({message:"Bad request"});
        } else {
            const provider = providerRepository.addProvider(newProvider);
            res.status(201).json(provider).json({message:"Created"});
        }
    },

    updateProvider(req, res) {
        if(req.params.id == undefined) {
            res.status(400).json({message:"Bad request"});
        } else {
            let oldProvider = providerRepository.getProviderById(parseInt(req.params.id));
            if(oldProvider == null) {
                res.status(404).json({message:"Bad request"});
            } else {
                req.body.id = parseInt(req.params.id);
                let simProvider = checkDataForUpdate(oldProvider, req.body);
                if(simProvider == null) {
                    res.status(400).json({message:"Bad request"});
                } else {
                    let resProvider = providerRepository.updateProvider(simProvider);
                    res.status(200).json(resProvider);
                }
            }
        }
    },

    deleteProvider(req, res) {
        const providerId = req.params.id;
        let provider = providerRepository.deleteProvider(parseInt(providerId));
        if(provider != null) {
            res.status(200).json(provider);
        } else {
            res.status(404).json({ message:"Not Found" });
        }
    },
};