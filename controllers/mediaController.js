const MediaRepository = require('../repositories/mediaRepository');
const path = require('path');
const mediaRepository = new MediaRepository(path.resolve(__dirname, '../data/media'));
const multer = require('multer');

const upload = multer({

    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, mediaRepository.filepath);
        },
        filename: function (req, file, cb) {
            cb(null, `${String(mediaRepository.getNextId())}.${file.mimetype.split('/')[1]}`);
        }
    }),
}).any();

module.exports = {
    getMediaById(req, res) {
        let image = mediaRepository.getMediaPath(parseInt(req.params.media_id));
        if(image !== undefined) {
            console.log (typeof(image));
            res.status(200).sendFile(image);
        } else {
            res.status(404).json({ message:"Not found" });
        }
    },

    addMedia(req, res) {
        upload(req, res, (err) => {
            if (err) {
                console.log('err ', err.message);
                res.status(500).json({ message: 'Internal Server Error' });
                return;
            } else if (req.files) {
                res.status(201).json({ id: mediaRepository.getNextId() });
                mediaRepository.incrementNextId();
            } else {
                res.status(400).json({ message: 'Bad request' });
            }
        });
    },
};
