const Provider = require('../models/provider');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    addProvider(item) {
        let items = this.storage.readItems().items;
        let newItem = new Provider(this.storage.nextId, item.name, item.connectionType, item.price, item.rating, item.date);
        this.storage.incrementNextId();
        items.push(newItem);
        this.storage.writeItems(items);
        return newItem;
    }
 
    getProviders() { 
        return this.storage.readItems().items;
    }
 
    getProviderById(id) {
        const items = this.storage.readItems().items;
        for (const item of items) {
            if (item.id === id) {
                return new Provider(item.id, item.name, item.connectionType, item.price, item.rating, item.date);
            }
        }
        return null;
    }

    updateProvider(item) {
        let isDeleted = this.deleteProvider(item.id);
        if(isDeleted) {
            let items = this.storage.readItems().items;
            items.push(item);
            this.storage.writeItems(items);
            return item;
        } else {
            return null;
        }
    }

    deleteProvider(id) {
        let items = this.storage.readItems().items; 
        let item = this.getProviderById(id);
        if((item)) {
            let indexOfItem = items.indexOf(item);
            items.splice(indexOfItem, 1);
            this.storage.writeItems(items);
            return item;
        } else {
            return null;
        }
    }
};
 
module.exports = UserRepository;