const JsonStorage = require('../jsonStorage');
const fs = require('fs');

class mediaRepository {
    
    constructor(filepath) {
        this.filepath = filepath;
        this.storage = new JsonStorage(this.filepath + '.json');
    }

    getNextId() {
        return this.storage.readItems().nextId;
    }

    incrementNextId() {
        const items = this.storage.readItems().items;
        this.storage.incrementNextId();
        this.storage.writeItems(items);
    }

    getMediaPath(id) {
        for(const format of this.storage.readItems().fileFormats) {
            const path = this.filepath + "\\" + String(id) + '.' + format;
            if(fs.existsSync(path)) {
                return path;
            }
        }
        return undefined;
    }
};

module.exports = mediaRepository;